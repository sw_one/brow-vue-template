import { transform as _t } from './modules/moduleB.js'

let data = [ 0, 1, 2, 3 ,4, 5, 6, 7 ];

data = data.map((item) => { return item + _t });

export { data }
