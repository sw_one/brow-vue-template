import Vue from 'vue/dist/vue.js'

let vapp = new Vue({
    //el: '#vue-app',
    data: function () {
        return {
            setup: [ 'a', 'b', 'c' ]
        }
    },
    template:
        `
    <div>Test Vue<br>
        <template v-for="(item, index) in setup">
            <span v-bind:data-key="index">{{ index }} : {{ item }}</span><br>
        </template>
    </div>
    `
});


export { vapp }