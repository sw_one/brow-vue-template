let gulp = require('gulp');
let babelify = require('babelify');
let browserify = require('browserify');
let source = require('vinyl-source-stream');
let buffer = require('vinyl-buffer');

gulp.task('build', () => {

    browserify('src/entryPoint.js')
        .transform(babelify, { presets: [ 'es2015' ] })
        .bundle()
        .pipe(source('bundle.js'))
            .pipe(gulp.dest('dist'))
            .pipe(buffer());     // You need this if you want to continue using the stream with other plugins

});

gulp.task('watch', function() {
    gulp.watch(['src/*.js', 'src/**/*.js', 'src/lib/*.js'], ['build']);
});

